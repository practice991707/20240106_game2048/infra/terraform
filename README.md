# Terraform для разворачивания тестового проекта на базе Yandex облака


* Поднимается балансировщик и два экземпляра приложения на базе Container Optimized Image (COI). COI конфигурируются через cloud_config.yaml чере cloud-init.
Запуск приложения осуществляется на базе docker-compose.yaml. В качестве выходных переменных (output) будет получен IP адрес балансировщика для подключения.

* Для корректной работы необходимо изменить cloud_id и folder_id из файла параметров tfvars на ваш и поместить токен для доступа к облаку в файл "./tf_key.json".
* Вынесены в переменные zone, instance_count (число создаваемых экземпляров) и target_port (конечный порт приложения, на который ссылается lb)

```
terraform plan
terraform apply
terraform apply -var-file="testing.tfvars"
```

License
-------

GPLv3

Author Information
------------------

* Kirill Fedorov (tg:fedrr) as a challege for ClassBook #240106 project https://deusops.com/classbook