variable "folder_id" {
  type        = string
  description = "Folder id for cloud instance"
  sensitive   = true
}

variable "cloud_id" {
  type        = string
  description = "Default cloud_id for resources"
  sensitive   = true
}

variable "zone" {
  type        = string
  description = "Default zone for resources"
  default     = "ru-central1-a"
}

variable "instance_count" {
  type        = number
  description = "Default instance count"
  default     = 2
}

variable "target_port" {
  type        = number
  description = "Default target port"
  default     = 80
}
