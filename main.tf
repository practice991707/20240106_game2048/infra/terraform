resource "yandex_vpc_network" "foo" {}

resource "yandex_vpc_subnet" "foo" {
  zone           = var.zone
  network_id     = yandex_vpc_network.foo.id
  v4_cidr_blocks = ["10.5.0.0/24"]
  route_table_id = yandex_vpc_route_table.rt.id
}

resource "yandex_vpc_gateway" "nat_gateway" {
  name = "internet"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "rt" {
  name       = "test-route-table"
  network_id = yandex_vpc_network.foo.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.nat_gateway.id
  }
}

resource "yandex_iam_service_account" "service-accounts" {
  for_each = local.service-accounts
  name     = each.key
}
resource "yandex_resourcemanager_folder_iam_member" "game-roles" {
  for_each  = local.game-sa-roles
  folder_id = local.folder_id
  member    = "serviceAccount:${yandex_iam_service_account.service-accounts["game-sa-super"].id}"
  role      = each.key
}

resource "yandex_resourcemanager_folder_iam_member" "game-ig-roles" {
  for_each  = local.game-ig-sa-roles
  folder_id = local.folder_id
  member    = "serviceAccount:${yandex_iam_service_account.service-accounts["game-ig-sa-super"].id}"
  role      = each.key
}


data "yandex_compute_image" "coi" {
  family = "container-optimized-image"
}

resource "yandex_compute_instance_group" "game-1" {
  service_account_id = yandex_iam_service_account.service-accounts["game-ig-sa-super"].id
  name               = "game-1"
  instance_template {
    service_account_id = yandex_iam_service_account.service-accounts["game-sa-super"].id
    resources {
      cores         = 2
      memory        = 1
      core_fraction = 5
    }

    network_interface {
      network_id = yandex_vpc_network.foo.id
      subnet_ids = ["${yandex_vpc_subnet.foo.id}"]
    }
    boot_disk {
      initialize_params {
        type     = "network-hdd"
        size     = "30"
        image_id = data.yandex_compute_image.coi.id
      }
    }

    scheduling_policy {
      preemptible = true
    }

    metadata = {
      docker-compose = file("${path.module}/config/docker-compose.yaml")
      user-data = file("${path.module}/config/cloud_config.yaml")
    }
  }

  scale_policy {
    fixed_scale {
      size = var.instance_count
    }
  }

  allocation_policy {
    zones = [var.zone]
  }
  deploy_policy {
    max_unavailable = 1
    max_creating    = var.instance_count
    max_expansion   = var.instance_count
    max_deleting    = var.instance_count
  }

  load_balancer {
    target_group_name        = "target-group"
    target_group_description = "load balancer target group"
  }
}

resource "yandex_lb_network_load_balancer" "lb-1" {
  name = "network-load-balancer-1"

  listener {
    name        = "network-load-balancer-1-listener"
    port        = var.target_port
    target_port = var.target_port
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.game-1.load_balancer.0.target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = var.target_port
      }
    }
  }
}