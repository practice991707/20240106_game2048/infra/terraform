locals {
  service-accounts = toset([
    "game-sa-super",
    "game-ig-sa-super",
  ])
  game-sa-roles = toset([
    "container-registry.images.puller",
    "monitoring.editor",
  ])
  game-ig-sa-roles = toset([
    "compute.editor",
    "iam.serviceAccounts.user",
    "load-balancer.admin",
    "vpc.publicAdmin",
    "vpc.user",
  ])
}